require "rubygems"
require 'webdrivers'
require 'capybara'
require 'capybara/cucumber'
require 'report_builder'


Capybara.register_driver :chrome do |app|
    Capybara::Selenium::Driver.new(app, browser: :chrome)
end
  
Capybara.register_driver :headless_chrome do |app|
    capabilities = Selenium::WebDriver::Remote::Capabilities.chrome(
      chromeOptions: {
        args: %w[headless enable-features=NetworkService,NetworkServiceInProcess]
      }
    )
  
    Capybara::Selenium::Driver.new app,
      browser: :chrome,
      desired_capabilities: capabilities
end

Capybara.configure do |config|
    config.default_driver = :chrome #roda no navegador
    Capybara.default_max_wait_time = 20
    #config.default_driver = :headless_chrome #roda com o navegador em background
end